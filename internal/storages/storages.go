package storages

import (
	"gitlab.com/antoxa2614/goboilerplate/internal/db/adapter"
	"gitlab.com/antoxa2614/goboilerplate/internal/infrastructure/cache"
	vstorage "gitlab.com/antoxa2614/goboilerplate/internal/modules/auth/storage"
	ustorage "gitlab.com/antoxa2614/goboilerplate/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
